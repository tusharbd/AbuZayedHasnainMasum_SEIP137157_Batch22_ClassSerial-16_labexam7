<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\ProfilePicture\ProfilePicture;

$student_photo= new ProfilePicture();


$singleItem = $student_photo->prepare($_GET)->view();

unlink('../../../resource/images/ProfilePictures/'.$singleItem->photo_field);

$student_photo->prepare($_GET)->delete();
