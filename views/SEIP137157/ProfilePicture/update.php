<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\ProfilePicture\ProfilePicture;

$student_photo= new ProfilePicture();
$student_photo->prepare($_REQUEST);
$singleItem= $student_photo->view();

if((isset($_FILES['student_photo'])&& !empty($_FILES['student_photo']['name']))){
    $image_name= time().$_FILES['student_photo']['name'];
    $temporary_location= $_FILES['student_photo']['tmp_name'];
    unlink('../../../resource/images/ProfilePictures/'.$singleItem->photo_field);
    move_uploaded_file( $temporary_location,'../../../resource/images/ProfilePictures/'.$image_name);
    $_REQUEST['student_photo']=$image_name;

}



$student_photo->prepare($_REQUEST)->update();
