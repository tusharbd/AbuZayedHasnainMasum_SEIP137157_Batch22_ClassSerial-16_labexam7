<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\Utility\Utility;
use App\Bitm\SEIP137157\Message\Message;
use App\Bitm\SEIP137157\ProfilePicture\ProfilePicture;


$student_photo= new ProfilePicture();


$student_photo->prepare($_GET);

$singleItem= $student_photo->view();
if($singleItem==NULL)Utility::redirect("index.php");


?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body background="../../../resource/images/bgimage.png">

<div class="container">
    <h2>Update Profile Picture : <?php echo $singleItem->name_field; if($singleItem->deleted_at != NULL) echo "  [ Trashed ]" ;  ?>  </h2>

    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $singleItem->id?>">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control"name="student_name" value="<?php echo $singleItem->name_field?>" >
        </div>
        <div class="form-group">
            <label for="pwd">Upload Profile Picture:</label>
            <input type="file" name="student_photo" class="form-control">
            <br>


            <input class="btn-lg btn-primary" type="submit" value="Update">
            


            <div id="message" >

                <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                    echo "&nbsp;".Message::message();
                }
                Message::message(NULL);

                ?>
            </div>



            <br> <br> <br>

            <img  src="../../../resource/images/ProfilePictures/<?php echo $singleItem->photo_field?>"
        </div>
    </form>
</div>

</body>



<script>
    $('#message').show().delay(1200).fadeOut();

    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete ID# "+id+" ?");
        if (x)
            return true;
        else
            return false;
    }

</script>



</html>







