<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\ProfilePicture\ProfilePicture;
use App\Bitm\SEIP137157\Utility\Utility;
use App\Bitm\SEIP137157\Message\Message;


$profilepicture = new ProfilePicture();
$recordSet =  $profilepicture->trashed();




?>

<!DOCTYPE html>

<head>
    <title>Trashed List - Profile Pictures</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.min.js"></script>
</head>
<body background="../../../resource/images/bgimage.png">



<div class="container">
    <h2>Trashed List of Profile Pictures</h2>
    <form action="recovermultiple.php" method="get" id="multiple">
        <table>
            <tr>
                <td height="100">
                    <div id="TopMenuBar">
                        <button type="button" onclick="window.location.href='create.php'" class=" btn-primary btn-lg">Add new</button>
                        <button type="button" onclick="window.location.href='index.php'" class="btn-lg btn-success">Active List</button>
                        <button type="submit"  class="btn-lg  btn-info">Recover Selected</button>
                        <button type="button"  class="btn-lg btn-danger" id="multiple_delete">Delete Selected</button>

                    </div>
                </td>

                <td width = "50">

                </td>

                <td height="100" >

                    <div id="message" >

                        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                            echo "&nbsp;".Message::message();
                        }
                        Message::message(NULL);

                        ?>
                    </div>

                </td>
            </tr>
        </table>





        <div class="table-responsive">

            <table class="table">
                <thead>
                <tr>
                    <th width = "150"> <input type="checkbox"  id="checkall">  Check Item(s)</th>
                    <th>Serial</th>
                    <th>ID</th>
                    <th>Full Name</th>
                    <th>Photo</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>



                <?php
                $serial = 0;
                while($row = $recordSet->fetch_assoc() ) {



                    ?>



                    <tr <?php if($serial%2) echo 'bgcolor="#fffaf0"'; else echo 'bgcolor="#f8f8ff"';?>>
                        <td width = "100" align="center">
                            <input type="checkbox" name="mark[]" value="<?php echo  $row["id"]?>">
                        </td>


                        <td>
                            <?php echo ++$serial?>
                        </td>

                        <td >
                            <?php echo $row["id"]?>
                        </td>

                        <td>
                            <?php echo  $row["name_field"]?>
                        </td>


                        <td><img src="../../../resource/images/ProfilePictures/<?php echo $row['photo_field'] ?>" alt="image" height="100px" width="100px" class="img-responsive"> </td>



                        <td >
                            <a href="view.php?id=<?php echo $row["id"]?>" class="btn btn-primary" role="button">View</a>
                            <a href="edit.php?id=<?php echo $row["id"]?>"  class="btn btn-info" role="button">Edit</a>
                            <a href="recover.php?id=<?php echo $row["id"]?>" class="btn btn-success" role="button">Recover</a>
                            <a href="delete.php?id=<?php echo $row["id"]?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete(<?php echo $row["id"]?>)">Delete</a>

                        </td>
                    </tr>
                    <?php
                }// end of while
                ?>
                </tbody>
            </table>
    </form>
</div>
</div>


</body>

<script>

    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();



    
    function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete ID# "+id+" ?");
        if (x)
            return true;
        else
            return false;
    }




    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    $(document).ready(function() {
        $("#checkall").click(function() {
            var checkBoxes = $("input[name=mark\\[\\]]");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        });
    });




</script>


</HTML>


