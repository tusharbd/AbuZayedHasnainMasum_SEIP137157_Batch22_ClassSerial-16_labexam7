<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile Picture </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body background="../../../resource/images/bgimage.png">

<div class="container">
    <h2>Upload Profile Picture</h2>
    <form role="form" action="store.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Student Name:</label>
            <input type="text" class="form-control"name="student_name" >
        </div>
        <div class="form-group">
            <label for="pwd">Profile Picture:</label>
            <input type="file" name="student_photo" class="form-control">
        </div>

        <input class="btn-lg btn-primary" type="submit" value="Upload">

    </form>
</div>

</body>
</html>

