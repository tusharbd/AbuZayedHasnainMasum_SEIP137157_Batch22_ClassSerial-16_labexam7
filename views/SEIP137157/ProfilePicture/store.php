<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137157\Utility\Utility;
use App\Bitm\SEIP137157\ProfilePicture\ProfilePicture;


if((isset($_FILES['student_photo'])&& !empty($_FILES['student_photo']['name']))){
    $image_name= time().$_FILES['student_photo']['name'];
    $temporary_location= $_FILES['student_photo']['tmp_name'];

    move_uploaded_file( $temporary_location,'../../../resource/images/ProfilePictures/'.$image_name);
    $_REQUEST['student_photo']=$image_name;


}

$profile_picture= new ProfilePicture();
$profile_picture->prepare($_REQUEST)->store();

