<?php
namespace App\Bitm\SEIP137157\ProfilePicture;
use App\Bitm\SEIP137157\Utility\Utility;
use App\Bitm\SEIP137157\Message\Message;



class ProfilePicture{
    public $id="";
    public $student_name="";
    public $student_photo="";
    public $conn;

    public $refferarFilename;

    public function __construct()
    {
        if (isset($_SERVER['HTTP_REFERER']) ) {
            $this->refferarFilename = basename((string)$_SERVER['HTTP_REFERER']);
            if (strpos($this->refferarFilename, ".php") == false) $this->refferarFilename = "index.php";
        } else $this->refferarFilename = "index.php";
        
        $this->conn = mysqli_connect("localhost", "root", "", "labxm7b22") or die("Database connection failed");
    }
  
  
    public function prepare($data = "")
    {
        if (array_key_exists("student_name", $data)) {
            $this->student_name = $data['student_name'];
        }
        if (array_key_exists("student_photo", $data)) {
            $this->student_photo = $data['student_photo'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
      //  Message::message($this);
        return  $this;

    }

    public function store(){
        $query="INSERT INTO `labxm7b22`.`profile_picture` (`name_field`, `photo_field`) VALUES ('".$this->student_name."', '".$this->student_photo."')";
     //     echo $query;
      //  die();
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }

    public function index(){

        $query = 'SELECT * FROM `profile_picture` WHERE `deleted_at` IS NULL';
        $recordSet = mysqli_query($this->conn, $query);
        return $recordSet;

    }





    public function view(){
        $query="SELECT * FROM `labxm7b22`.`profile_picture` WHERE `id`=".$this->id;
        // Utility::dd($query);

        $result = mysqli_query($this->conn,$query);

        $row = mysqli_fetch_object($result);
        return $row;
    }


    public function update(){
        if(!empty($this->student_photo)) {
            $query = "UPDATE `labxm7b22`.`profile_picture` SET `name_field` = '" . $this->student_name . "', `photo_field` = '" . $this->student_photo . "' WHERE `profile_picture`.`id` =" . $this->id;
        }
        else {
            $query = "UPDATE `labxm7b22`.`profile_picture` SET `name_field` = '" . $this->student_name ."' WHERE `profile_picture`.`id` =" . $this->id;

        }

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }


    public function delete(){
        $query="DELETE FROM `labxm7b22`.`profile_picture` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Failed!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect($this->refferarFilename);
        }

    }


    public function showActive(){
        $query="SELECT * FROM `labxm7b22`.`profile_picture` WHERE `make_active` IS NOT NULL";
        // Utility::dd($query);

        $result = mysqli_query($this->conn,$query);

        $row = mysqli_fetch_object($result);
        return $row;
    }


    public function makeActive()
    {

        $query = "UPDATE `labxm7b22`.`profile_picture` SET `make_active` = NULL"  ;
        $result = mysqli_query($this->conn, $query);



        $query = "UPDATE `labxm7b22`.`profile_picture` SET `make_active` = 'ACTIVE'  WHERE `profile_picture`.`id` = " . $this->id;
     //   echo $query;
      //  die();
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                     <div class=\"alert alert-info\">
                            <strong>Success!</strong> New Profile Picture Has Been Activated..
                     </div>");
            Utility::redirect($this->refferarFilename);
        } else {
            Message::message("
                     <div class=\"alert alert-info\">
                             <strong>Failed!</strong> New Profile Picture Not Activated .
                     </div>");
            Utility::redirect($this->refferarFilename);
        }
    }



    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `labxm7b22`.`profile_picture` SET `deleted_at` =" . $this->deleted_at . " WHERE `profile_picture`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                     <div class=\"alert alert-info\">
                            <strong>Trashed!</strong> Data has been trashed successfully.
                     </div>");
            Utility::redirect($this->refferarFilename);
        } else {
            Message::message("
                     <div class=\"alert alert-info\">
                             <strong>Failed!</strong> Data has not been trashed successfully.
                     </div>");
            Utility::redirect($this->refferarFilename);
        }
    }

    public function trashed()
    {
        $query = 'SELECT * FROM `profile_picture` WHERE `deleted_at` IS NOT NULL';
        $recordSet = mysqli_query($this->conn, $query);
        return $recordSet;
    }

    public function recover()
    {

        $query = "UPDATE `labxm7b22`.`profile_picture` SET `deleted_at` = NULL WHERE `profile_picture`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
              <div class=\"alert alert-info\">
                 <strong>Recovered!</strong> Data has been recovered successfully.
              </div>");
            Utility::redirect($this->refferarFilename);
        } else {
            Message::message("
              <div class=\"alert alert-info\">
                 <strong>Failed!</strong> Data has not been recovered successfully.
              </div>");
            Utility::redirect($this->refferarFilename);
        }

    }

    public function recoverSelected($IDs = Array())
    {

        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `labxm7b22`.`profile_picture` SET `deleted_at` = NULL WHERE `profile_picture`.`id` IN(" . $ids . ")";

            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                  <div class=\"alert alert-success\">
                        <strong>Recovered!</strong> Selected Data has been recovered successfully.
                  </div>");
                Utility::redirect($this->refferarFilename);
            } else {
                Message::message("
                  <div class=\"alert alert-info\">
                       <strong>Failed!</strong> Selected Data has not been recovered successfully.
                 </div>");
                Utility::redirect($this->refferarFilename);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");
            Utility::redirect($this->refferarFilename);
        }
    }





    public function trashSelected($IDs = Array())
    {

        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `labxm7b22`.`profile_picture` SET `deleted_at` = 'trashed multiple' WHERE `profile_picture`.`id` IN(" . $ids . ")";

            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                  <div class=\"alert alert-success\">
                        <strong>Trashed!</strong> Selected Data has been trashed successfully.
                  </div>");
                Utility::redirect($this->refferarFilename);
            } else {
                Message::message("
                  <div class=\"alert alert-danger\">
                       <strong>Failed!</strong> Selected Data has not been trashed successfully.
                 </div>");
                Utility::redirect($this->refferarFilename);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");
            Utility::redirect($this->refferarFilename);
        }
    }




    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);


            $query = "SELECT * FROM `labxm7b22`.`profile_picture` WHERE `profile_picture`.`id`  IN(" . $ids . ")";
            $recordSet = mysqli_query($this->conn, $query);

            while($row = $recordSet->fetch_assoc()) {
                unlink('../../../resource/images/ProfilePictures/'.$row['images']);

            }


            $query = "DELETE FROM `labxm7b22`.`profile_picture` WHERE `profile_picture`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                 <div class=\"alert alert-info\">
                      <strong>Deleted!</strong> Selected Data has been deleted successfully.
                 </div>");
                Utility::redirect($this->refferarFilename);
            } else {
                Message::message("
               <div class=\"alert alert-info\">
                   <strong>Failed!</strong> Selected Data has not been deleted successfully.
               </div>");
                Utility::redirect($this->refferarFilename);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");
            Utility::redirect($this->refferarFilename);
        }

    }



}



?>